(* Paolo Torrini, KU Leuven *)

Require Import JHFunctors1.
Require Import Aux2.
 

Lemma mendlerDInd (I: Type) (K: I -> Type) (F: ffPr I K) 
                  {Fun_F: DFunctor I K F} (P: Prop) :
         (DMAlgebra I K F (fun (i: I) (x: K i) => P)) -> 
                              forall (i: I) (w: K i), DMFix I K F i w -> P.
Proof.
intros f i w x.
apply (dmfold I K F (fun i x => P) f i w).
assumption.
Defined.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)
