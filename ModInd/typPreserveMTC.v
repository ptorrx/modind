(* Paolo Torrini, KU Leuven *)

Require Export Aux1.
Require Import rTyp.
Require Import Functors1.
Require Import Functors2.
Require Import JFunctors1.
Require Import Functors3.
Require Import rExp.
Require Import typPreserve.

(* example of type preservation proof based on MTC-syle induction *)


Definition NN: TVr := typPreserve.NN.

(* weak induction principle for expressions - ExpWeakInduction *)
Definition ind_alg_bNum
    (P : rNum -> Prop)
    (H1 : forall n, P (NNum (NLitT n)))
    (H2 : forall (e1 e2: rNum) 
      (IH1 : P e1)
      (IH2 : P e2),
      P (NNum (NSumT e1 e2)))
    (H3 : P (NNum NErrT))
    (e : bNum (sig P)) : sig P :=
    match e with
      | NLit n => exist P (NNum (NLitT n)) (H1 n)
      | NSum a1 a2 => exist P _
        (H2 (proj1_sig a1) (proj1_sig a2) (proj2_sig a1) (proj2_sig a2))
      | NErr => exist P _ H3 
    end.


Lemma wf_ind_PAlg_bNum 
    (P : rNum -> Prop)
    (H1 : forall n, P (NNum (NLitT n)))
    (H2 : forall (e1 e2: rNum) 
      (IH1 : P e1)
      (IH2 : P e2),
      P (NNum (NSumT e1 e2)))
    (H3 : P (NNum NErrT)) :
    PAlgebra bNum P (ind_alg_bNum P H1 H2 H3).
    econstructor.
    destruct e; simpl.
    reflexivity.
    reflexivity.
    reflexivity.
Defined.


Lemma wf_typPresAlgMTC 
    (H1 : forall n, typPreserve (NNum (NLitT n)))
    (H2 : forall (e1 e2: rNum) 
      (IH1 : typPreserve e1)
      (IH2 : typPreserve e2),
      typPreserve (NNum (NSumT e1 e2)))
    (H3 : typPreserve (NNum NErrT)) : 
             PAlgebra bNum typPreserve (ind_alg_bNum typPreserve H1 H2 H3). 
   apply wf_ind_PAlg_bNum.
Defined.

(*
Global Instance bNum2typPreserve_PAlgebra : 
        PAlgebra bNum typPreserve typPresAlgMTC :=
{| WF_MInd := wfTypPreserve
|}.
*)


Lemma typPresNLit : forall n, typPreserve (NNum (NLitT n)).
intros.
unfold typPreserve.
intros.
simpl.
assumption.
Defined.

Lemma typPresNSum 
            {Fun_F1: @FunctorSQU bNum bNum_Functor}
            {Fun_F2: @FunctorSQU bTyp bTyp_Functor}: forall (e1 e2: rNum) 
      (IH1 : typPreserve e1)
      (IH2 : typPreserve e2),
      typPreserve (NNum (NSumT e1 e2)).
unfold typPreserve.
intros.
inversion H; subst.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H1; subst.
inversion H1.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H0; subst.
inversion H0; subst.
apply (jmOut (rNum * rTyp) bTyping) in H2.
apply (jmOut (rNum * rTyp) bTyping) in H3.
apply (IH1 TNN) in H2.
apply (IH2 TNN) in H3.
eapply sumTypLemma.
assumption.
assumption.
assumption.
assumption.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H1; subst.
inversion H1.
Defined.

Lemma typPresNErr : typPreserve (NNum NErrT).
unfold typPreserve.
auto.
Defined.


Definition typPresAlgMTC1 
            {Fun_F1: @FunctorSQU bNum bNum_Functor}
            {Fun_F2: @FunctorSQU bTyp bTyp_Functor} : 
 Algebra bNum (sig typPreserve) :=
    ind_alg_bNum typPreserve typPresNLit typPresNSum typPresNErr.

Lemma wf_typPresAlgMTC1 
            {Fun_F1: @FunctorSQU bNum bNum_Functor}
            {Fun_F2: @FunctorSQU bTyp bTyp_Functor} : 
      PAlgebra bNum typPreserve typPresAlgMTC1.
apply wf_typPresAlgMTC.
Defined.

(* main result - subject reduction *)
Lemma subject_reduction_MTC 
                   {Fun_F1: @FunctorSQU bTyp bTyp_Functor}
                   {Fun_F2: @FunctorSQU bNum bNum_Functor}
                   {fUP' : forall e : rNum, Universal_Property'_fold e}  
                   (e: rNum) : typPreserve e.
Proof.
apply (@IndMTC bNum bNum_Functor typPreserve typPresAlgMTC1 wf_typPresAlgMTC1). auto. 
Defined.
                 

(************************************)

Lemma typPresAlgMTC : 
           MAlgebra bNum (sig typPreserve).
Proof. 
unfold MAlgebra.
unfold Mixin.
intros R rec e.
unfold typPreserve.
unfold typPreserve in rec.
(**)
econstructor.
intros t.
instantiate (1 := (NNum (NErr rNum))).
simpl.
intros.
assumption.
Defined.


Lemma typPreserve_MA_inst : 
    forall (A: Set) (e: bNum A) (rec: A -> sig typPreserve),
           typPresAlgMTC A rec e = typPresAlgMTC (sig typPreserve) id 
                   (fmap rec e). 
Proof.
intros.
destruct e.
reflexivity.
reflexivity.
reflexivity.
Defined.


Global Instance bNum2typPreserve_MendlerAlgebra : 
        MendlerAlgebra (sig typPreserve) typPresAlgMTC :=
{| ME_1 := typPreserve_MA_inst
|}.



(********************************)


Lemma typPresMTC {xUP: forall x : rNum, Universal_Property'_mfold x}
                 {MPA: MPAlgebra bNum typPreserve typPresAlgMTC} :
                     forall w: rNum, typPreserve w. 
Proof.
intro w.
apply (@Ind bNum bNum_Functor typPreserve typPresAlgMTC 
       MPA bNum2typPreserve_MendlerAlgebra w (xUP w)).
Defined.



(*** not needed *)
Lemma typPresAlgMTC2 {Fun_F1: @FunctorSQU bTyp bTyp_Functor}
                    {Fun_F2: @FunctorSQU bNum bNum_Functor} : 
           MAlgebra bNum (sig typPreserve).
Proof. 
unfold MAlgebra.
unfold Mixin.
intros R rec e.
unfold typPreserve.
unfold typPreserve in rec.
inversion e; subst.
econstructor.
intros t.
(* Show Existentials. *)
instantiate (1 := (NNum (NLit rNum n))).
intros.
simpl.
assumption.
(**)
apply rec in e1.
apply rec in e2.
elim e1.
elim e2.
intros x1 Hx1 x2 Hx2.
econstructor.
intros t.
specialize (Hx1 t).
specialize (Hx2 t). 
(* Show Existentials. *)
instantiate (1 := (NNum (NSum rNum x1 x2))).
intros.
inversion H; subst.
simpl.
constructor.
apply (@mInInvertEq bNum bNum_Functor Fun_F2) in H0; subst.
inversion H0; subst.
apply (jmOut (rNum * rTyp) bTyping) in H3.
apply (jmOut (rNum * rTyp) bTyping) in H2.
apply Hx1 in H2.
apply Hx2 in H3.
apply sumTypLemma.
assumption.
assumption.
assumption.
assumption.
simpl.
constructor.
(**)
econstructor.
intros t.
instantiate (1 := (NNum (NErr rNum))).
simpl.
intros.
assumption.
Defined.



(********************************)

Lemma typPresCAlgMTC : 
           Algebra bNum (sig typPreserve).
Proof. 
unfold Algebra.
intros H.
unfold typPreserve.
unfold typPreserve in H.
(**)
econstructor.
intros t.
instantiate (1 := (NNum (NErr rNum))).
simpl.
intros.
assumption.
Defined.



(********************************)

Definition bTypingF (A: Set) (rec: A -> option rTyp) 
                    (e: bNum A) : option rTyp :=
  match e with
  | NLit n => Some TNN
  | NSum e1 e2 => match (rec e1) with
                  | Some X => match mOut bTyp X with 
                              | TId _ => match (rec e2) with 
                                     | Some Y => match mOut bTyp Y with
                                           | TId _ => Some TNN
                                           | _ => None
                                           end
                                     | _ => None
                                     end
                              | _ => None  
                              end
                  | _ => None
                  end
  | NErr => Some (mIn bTyp (TBot rTyp))
  end.

Definition rTypingF (e: rNum) : option rTyp :=
  mfold (option rTyp) bTypingF e.


Definition typPreserveMTC (e: rNum) : Prop := forall t: rTyp, 
    rTypingF e = Some t -> rTypingF (val2num (rEval e)) = Some t.


Lemma typPresCAlgMTC2 : 
           Algebra bNum (sig typPreserveMTC).
Proof. 
unfold Algebra.
intros H.
unfold typPreserveMTC.
unfold typPreserveMTC in H.
(**)
econstructor.
intros t.
instantiate (1 := (NNum (NErr rNum))).
simpl.
intros.
assumption.
Defined.




(*****************************************)

(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)