
Require Import FunctionalExtensionality.

Require Import Functors1.
Require Import JFunctors1.
Require Export Aux1.
Require Export Aux2.
Require Import Functors2.
Require Import JFunctors2.

(* Partly a refactoring from the MTC distribution, 
   by Paolo Torrini, KU Leuven *)

(*
classes: FoldUniversalProperty
         MFoldUniversalProperty
         StrongMendlerAlgebra
         WellFormedProofAlgebra 
         WellFormedProofMAlgebra 
*)
(************************************)


Lemma Universal_Property_fold (F : Set -> Set) {fun_F : Functor F} 
    (B : Set)
    (f : Algebra F B) : forall (h : Fix F -> B), 
             h = fold_ B f ->
             forall e: F (Fix F), h (in_t e) = f (fmap h e).
intros. 
rewrite H.
reflexivity.
Defined.

(* class FoldUniversalProperty for Church encodings *)
Class Universal_Property'_fold {F} {fun_F : Functor F} (x : Fix F) :=
    {E_fUP' : forall (B : Set) (f : Algebra F B) (h : Fix F -> B),
      (forall e,  h (in_t e) = f (fmap h e)) ->
                            h x = fold_ _ f x
    }.

Lemma Fix_id_fold F {fun_F : Functor F} (e: Fix F) 
                 {UP' : Universal_Property'_fold e} :
    fold_ _ (@in_t F) e = e.
    intros; apply sym_eq.
    fold (id e); unfold id at 2; apply (E_fUP'); intros.
    rewrite fmap_id.
    unfold id.
    reflexivity.
Defined.


Lemma Fusion F {fun_F : Functor F} 
                 (e: Fix F) 
                 {e_UP' : Universal_Property'_fold e} :
    forall (A B : Set) 
           (h : A -> B) 
           (f : Algebra F A) (g : Algebra F B),
      (forall a: F A, h (f a) = g (fmap h a)) ->
        (fun (e': Fix F) => h (fold_ A f e')) e = fold_ B g e.
    intros; eapply E_fUP'; try eassumption; intros.
    rewrite (Universal_Property_fold F _ f _ (refl_equal _)).
    rewrite H.
    rewrite fmap_fusion; reflexivity.
Defined.

(**************************************************)


Lemma Universal_WProperty_mfold (F : Set -> Set) 
    (B : Set)
    (f : MAlgebra F B) : forall (h : MFix F -> B), 
             h = mfold B f ->
             forall e: F (MFix F), h (mIn F e) = f (MFix F) h e.
intros. 
rewrite H.
reflexivity.
Defined. 


(* class MFoldUniversalProperty for Mendler encodings *)
Class Universal_Property'_mfold {F} {fun_F : Functor F} (x : MFix F) :=
    { ME_fUP' : forall (B : Set) (f : MAlgebra F B) (h : MFix F -> B),
        (forall e: F (MFix F),   h (mIn F e) = f (MFix F) h e) ->  
                                     h x = mfold B f x 
    }.


(* class StrongMendlerAlgebra *)
Class MendlerAlgebra {F} {fun_F : Functor F} (B: Set) (f : MAlgebra F B) :=
    { ME_1: forall (A : Set) (e: F A) (rec: A -> B),
              f A rec e = f B id (fmap rec e) 
    }.


Lemma ME2_fUP' {F} {fun_F : Functor F} 
     (x : MFix F) {UP: Universal_Property'_mfold x}
     (B : Set) (f : MAlgebra F B) {ME: MendlerAlgebra B f} : 
     forall (h : MFix F -> B),
        (forall e: F (MFix F), h (mIn F e) = f B id (fmap h e)) ->  
                                     h x = mfold B f x.
Proof.
intros.
eapply ME_fUP'.
intros.
rewrite ME_1.
auto.
Defined.


Lemma ME_2  {F} {fun_F : Functor F} (B : Set) (f : MAlgebra F B)
            {ME: MendlerAlgebra B f} : 
        forall (e: F (MFix F)),
              f (MFix F) (mfold B f) e = f B id (fmap (mfold B f) e).
Proof.
intros.
apply ME_1.
Defined.


Lemma MendlerBasic1 F {fun_F: Functor F} (C: Set) (alg: MAlgebra F C) 
  (x: F (Fix F)) : 
      mfold C alg (mIn F x) = alg (MFix F) (mfold C alg) x.
Proof.
unfold mfold.
unfold mIn.
reflexivity.
Defined.


Lemma MendlerBasic1_2 F {fun_F: Functor F} (alg: MAlgebra F (MFix F)) 
  (x: F (Fix F)) : 
      mfold (MFix F) alg (mIn F x) = alg (MFix F) (mfold (MFix F) alg) x.
Proof.
apply MendlerBasic1.
auto.
Defined.


Lemma MFix_id_fold F {fun_F : Functor F} (e: MFix F) 
                   {UP' : Universal_Property'_mfold e} :
    mfold (MFix F) 
          (fun (A: Set) (r: A -> MFix F) (d: F A) => mIn F (fmap r d)) 
          e = e.
    intros.
    apply sym_eq.
    fold (id e); unfold id at 2; apply (ME_fUP'); intros.
    rewrite fmap_id.
    unfold id.
    reflexivity.
Defined.

Lemma MFix_id_foldG F {fun_F : Functor F} 
                   {UP' : forall e: MFix F, Universal_Property'_mfold e} :
    mfold (MFix F) 
       (fun (A: Set) (r: A -> MFix F) => (compose (mIn F) (fmap r)))  
      = @id (MFix F).
Proof.
apply functional_extensionality.
intros.
apply MFix_id_fold.
auto.
Defined.


Lemma Universal_Property_mfold (F : Set -> Set) {fun_F : Functor F} 
    (B : Set)
    (f : MAlgebra F B) 
    {ME: MendlerAlgebra B f} :  
         forall (h : MFix F -> B), 
             h = mfold B f ->
             forall e: F (MFix F),  h (mIn F e) = f B id (fmap h e).
intros. 
rewrite H.
unfold mfold at 1.
unfold mIn.
apply ME_1.
Defined.

Lemma MendlerBasic2 F {fun_F: Functor F}   
  (alg: MAlgebra F (MFix F)) {ME: MendlerAlgebra (MFix F) alg}
  (x: F (Fix F)) : 
      mfold (MFix F) alg (mIn F x) = 
            alg (MFix F) id (fmap (mfold (MFix F) alg) x).
Proof.
unfold mfold.
unfold mIn.
apply ME_1.
Defined.
 

Lemma MFix_id_mfix F {fun_F : Functor F} (x: MFix F) 
                   {fun_F2 : FunctorSQU F} :
    x = mIn F (fmap id (mOut F x)). 
Proof.
assert (forall a, fmap (@id (Fix F)) a = a).
apply fmap_id.
specialize (H (mOut F x)).
rewrite H at 1.
rewrite biject2.
reflexivity.
Defined.


(*************************)


Lemma MFusion F {fun_F : Functor F} 
              (e: MFix F) 
              {e_UP' : Universal_Property'_mfold e} 
              (A B : Set) (f : MAlgebra F A) (g : MAlgebra F B)
              {M1: MendlerAlgebra A f} 
              {M2: MendlerAlgebra B g} :
    forall (h : A -> B),
       (forall a: F A, h (f A id a) = g A h a) ->
(*      (forall a: F A, h (f a) = g (fmap h a)) -> *) 
         (fun (e': MFix F) => h (mfold A f e')) e = mfold B g e.
    intros.
    eapply ME2_fUP'.
    assumption.
    assumption.
    intros.
    rewrite (Universal_Property_mfold F _ f _ (refl_equal _) _).
    rewrite H.
    rewrite ME_1.
    rewrite fmap_fusion.
    reflexivity.
Defined.


(* class WellFormedProofAlgebra *)
Class PAlgebra (F: Set -> Set) {Fun_F : Functor F} 
    (P : Fix F -> Prop) 
    (p_algebra : Algebra F (sig P)) : Set :=
    { WF_Ind : forall e: F (sig P), proj1_sig (p_algebra e) =
      in_t (@fmap F Fun_F (sig P) (Fix F) (@proj1_sig (Fix F) P) e)}.


(* class WellFormedProofMAlgebra *)
Class MPAlgebra (F: Set -> Set) {Fun_F : Functor F} 
    (P : MFix F -> Prop) 
    (p_algebra : MAlgebra F (sig P))
     : Set :=
    { WF_MInd : forall e: F (sig P), 
                proj1_sig (p_algebra (sig P) id e) =
      mIn F (@fmap F Fun_F (sig P) (MFix F) (@proj1_sig (MFix F) P) e)}.

(* class WellFormedProofMAlgebra - alternative formulation (not used) *)
Class MPAlgebra2 (F: Set -> Set) {Fun_F : Functor F} 
    (P : MFix F -> Prop) 
    (p_algebra : MAlgebra F (sig P))
     : Set :=
    { WF_MInd2 : forall e: F (sig P), 
                proj1_sig (p_algebra (sig P) id e) =
      mIn F (@fmap F Fun_F (sig P) (Fix F) (@proj1_sig (Fix F) P) e)}.



Lemma ME_inst (F: Set -> Set) (fun_F : Functor F): 
      forall (A: Set) (e: F A) (rec: A -> MFix F),
           (fun (A: Set) (r: A -> MFix F) (d: F A) => 
                    mIn F (fmap r d)) A rec e = 
          (fun (A: Set) (r: A -> MFix F) (d: F A) => 
                    mIn F (fmap r d)) (MFix F) id (fmap rec e). 
Proof.
intros.
simpl.
rewrite fmap_id.
reflexivity.
Defined.

Global Instance inst1_MendlerAlgebra (F: Set -> Set) (fun_F : Functor F) : 
        MendlerAlgebra (MFix F) 
             (fun (A: Set) (r: A -> MFix F) (d: F A) => mIn F (fmap r d)) :=
{| ME_1 := ME_inst F fun_F
|}.
     

(* Mendler Sigma induction - MSigmaInduction *)
Lemma Ind (F : Set -> Set)
      {Fun_F : Functor F}
      {P : MFix F -> Prop}
      {p_alg : MAlgebra F (sig P)}
      {MPA : MPAlgebra F P p_alg}
      {M1: MendlerAlgebra (sig P) p_alg}
      :
      forall (x : MFix F) 
        (fUP' : Universal_Property'_mfold x), 
        P x.
Proof.
      intros.
      cut (proj1_sig (mfold _ p_alg x) = id x).
      unfold id.
      intro f_eq; rewrite <- f_eq.
      eapply (proj2_sig (mfold _ p_alg x)).
      erewrite (@MFusion _ Fun_F x fUP' _ _ p_alg (fun (A: Set) (r: A -> MFix F) (d: F A) => mIn F (fmap r d)) _ _ (@proj1_sig (Fix F) P)) at 1.
      eapply MFix_id_fold.
      assumption.
      intros.
      rewrite WF_MInd.
      simpl. 
      reflexivity.
Defined.


(* Conventional Sigma induction - SigmaInduction *)
Lemma IndMTC {F : Set -> Set}
      {Fun_F : Functor F}
      {P : Fix F -> Prop}
      (p_algebra: Algebra F (sig P))
      {Ind_Alg : PAlgebra F P p_algebra}
      :
      forall (f : Fix F)
        (fUP' : Universal_Property'_fold f),
        P f.
      intros.
      cut (proj1_sig (fold_ _ p_algebra f) = id f).
      unfold id.
      intro f_eq; rewrite <- f_eq.
      eapply (proj2_sig (fold_ _ p_algebra f)).
      erewrite (@Fusion _ Fun_F f fUP' _ _ (@proj1_sig (Fix F) P) p_algebra in_t).
      eapply Fix_id_fold; unfold id; assumption.
      intros; rewrite WF_Ind.
      simpl; unfold id; reflexivity.
    Defined.



(**************************************)


Lemma biject2lemma (F: Set -> Set) {Fun_F: Functor F} 
       (e: Fix F) (xUP: Universal_Property'_mfold e) : 
      mIn F (mOut F e) = e. 
Proof.
    intros.
    rewrite <- (@MFix_id_fold _ _ e xUP) at -1.
    eapply ME_fUP' with (h := fun e => mIn F (mOut F e)).
    intro.
    cut (mOut F (mIn F e0) = fmap (fun e1 => mIn F (mOut F e1)) e0). 
    intros.
    rewrite H; reflexivity.
    unfold mOut at 1.
    unfold mIn at 1.
    simpl.
    assert 
     (mfold (F (MFix F))
           (fun (A : Set) (f : A -> F (MFix F)) (a : F A) =>
            fmap (fun y0 : A => mIn F (f y0)) a)  =
       mOut F).
   unfold mOut.
   unfold mfold at 1.
   reflexivity.
   eauto.
Defined.


Lemma biject1lemma (F: Set -> Set) {Fun_F: Functor F} 
        (xUP: forall y, Universal_Property'_mfold y) (e: F (Fix F)): 
      mOut F (mIn F e) = e. 
Proof.
   assert (forall y, mOut F y = mfold (F (Fix F)) (fun A (f: A -> F (MFix F)) (a: F A) => fmap (fun (y:A) => mIn F (f y)) a) y).
    intros.
    reflexivity.    
    rewrite H.
    erewrite Universal_WProperty_mfold; try reflexivity.
    simpl. 
    assert ((fun y: MFix F => (mIn F (mfold (F (Fix F))
           (fun (A : Set) (f : A -> F (MFix F)) =>
            fmap (fun y0 : A => mIn F (f y0))) y)))   =  
                 fun y: MFix F => y).  
    apply functional_extensionality.
    intros.
    rewrite <- H.   
    apply biject2lemma.
    auto.
    rewrite H0 at 1.  
    rewrite fmap_id. 
    reflexivity.
Defined.


Lemma biject1lemmaP (F: Set -> Set) {Fun_F: Functor F} 
        (bij2: forall e, mIn F (mOut F e) = e) (e: F (Fix F)): 
      mOut F (mIn F e) = e. 
Proof.
   assert (forall y, mOut F y = mfold (F (Fix F)) (fun A (f: A -> F (MFix F)) (a: F A) => fmap (fun (y:A) => mIn F (f y)) a) y).
    intros.
    reflexivity.    
    rewrite H.
    erewrite Universal_WProperty_mfold; try reflexivity.
    simpl. 
    assert ((fun y: MFix F => (mIn F (mfold (F (Fix F))
           (fun (A : Set) (f : A -> F (MFix F)) =>
            fmap (fun y0 : A => mIn F (f y0))) y)))   =  
                 fun y: MFix F => y).  
    apply functional_extensionality.
    intros.
    rewrite <- H.   
    apply bij2.
    auto.
    rewrite H0 at 1.  
    rewrite fmap_id. 
    reflexivity.
Defined.


Global Instance inst1_SQUFunctor (F: Set -> Set) {fun_F : Functor F} 
        (xUP: forall y: MFix F, Universal_Property'_mfold y) : 
        FunctorSQU F :=
{| biject1 := biject1lemma F xUP;
   biject2 := fun y => biject2lemma F y (xUP y)
|}.
     


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)


