(* Paolo Torrini, KU Leuven *)

Require Import FunctionalExtensionality.

Require Import Aux2.


(* INDEXED ALGEBRAS for mutual induction *)
(*
Definition dSt (K: Type) : Type := K -> Set.

Definition fSt (K: Type) : Type := dSet K -> dSet K. 
*)


(* Church algebra *) 
Definition HAlgebra (K: Type) 
  (F: fSt K) (A: dSt K) : Set := forall (w:K), F A w -> A w.

(* Church fixpoint *)
Definition HFix (K: Type) 
   (F: fSt K) (w: K) : Set := 
      forall (A: dSt K), HAlgebra K F A -> A w.

(* Mixin *)
Definition HMixin (K: Type) 
    (R: dSt K) (F: fSt K) (A: dSt K) (v: K) := 
           (forall (w: K), R w -> A w) -> F R v -> A v.


(* Mendler algebra *)
Definition HMAlgebra (K: Type) (F: fSt K) (A: dSt K) : Set :=
    forall (R: dSt K) (v: K), HMixin K R F A v.

(* Mendler fixpoint *)
Definition HMFix (K: Type) (F: fSt K) (w: K) : Set :=
    forall (A: dSt K), HMAlgebra K F A -> A w.

(* Mendler fold *) 
Definition hmfold (K: Type) 
      (F: fSt K) : forall (A: dSt K) (f: HMAlgebra K F A) (w: K), 
           HMFix K F w -> A w := fun A f w e => e A f.


(* indexed functor class *)
Class HFunctor (K: Type) (F : fSt K) :=
    { hfmap :
        forall {A B : dSt K} (v:K), (forall (w:K), A w -> B w) ->  
                                                 F A v -> F B v;
      hfmap_fusion :
        forall (A B C: dSt K) (v:K) 
                              (f : forall (w:K), A w -> B w) 
                              (g : forall (w:K), B w -> C w)
                              (a: F A v),
          @hfmap B C v g (@hfmap A B v f a) = 
                  @hfmap A C v (fun w => fun e => g w (f w e)) a;
      hfmap_id :
        forall (A: dSt K) (w: K) (a: F A w),
          @hfmap A A w (fun w => @id (A w)) a = a
    }.



(* In morphism *)
Definition hmIn (K: Type) (F: fSt K) : 
        forall (w: K), F (HMFix K F) w -> HMFix K F w :=
    fun (w: K) (fe: F (HMFix K F) w) (A: dSt K) (f: HMAlgebra K F A) 
       => f (HMFix K F) w (hmfold K F A f) fe.


(* Out morphism, using Mendler fold *)
Definition hmOut (K: Type) (F : fSt K) {fun_F : HFunctor K F} (w: K)   
        (fe: HMFix K F w) : F (HMFix K F) w :=
                fe (F (HMFix K F)) 
                   (fun (R: dSt K) (w: K)
                        (rec: forall w0, R w0 -> (F (HMFix K F)) w0) 
                        (fa: F R w) =>
                  (@hfmap K F fun_F R (HMFix K F) w   
                       (fun w0 (x: R w0) => hmIn K F w0 (rec w0 x)) fa)).


(* Church fold *)
Definition hfold (K: Type) 
  (F: fSt K) {fun_F: HFunctor K F}:
    forall (A: dSt K) (f: HAlgebra K F A) (w: K), 
                    HMFix K F w -> A w :=
      fun (A: dSt K) (f: HAlgebra K F A) (w: K) (e: HMFix K F w) 
              => hmfold K F A 
                   (fun (R: dSt K) (w: K)
                        (rec: forall (v: K), R v -> A v) 
                        (fa: F R w) => f w (@hfmap K F fun_F R A w rec fa)) w e.


(* Out morphism, using Church fold *)
Definition hmOut_C (K: Type) 
     (F : fSt K) {fun_F : HFunctor K F} : 
        forall (w: K), HMFix K F w -> F (HMFix K F) w :=
    fun w => hfold K F (F (HMFix K F))
               (fun v => @hfmap K F fun_F 
                           (F (HMFix K F)) (HMFix K F) v (hmIn K F)) w.


 (* turns a Church fixpoint into a Mendler one *)
  Definition tHC2M (K: Type) (F: fSt K) (w: K) (x: HFix K F w) : 
                                                   HMFix K F w :=
    fun (C: dSt K) 
        (f: forall (A: dSt K) (w: K), (forall v: K, A v -> C v) -> 
                             (F A w -> C w)) => 
                 x C (fun w => f C w (fun v => @id (C v))).

 (* turns a Mendler fixpoint into a Church one *)
  Definition tHM2C (K: Type) (F: fSt K) {fun_F: HFunctor K F} (w: K)
                   (x: HMFix K F w) : HFix K F w :=
    fun (C: dSt K) (f: forall w: K, F C w -> C w) =>  
           x C (fun (A: dSt K) w (r: forall w: K, A w -> C w) y => 
                                     f w (@hfmap K F fun_F A C w r y)). 



(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)