(* Paolo Torrini, KU Leuven *)

Require Import HFunctors1.
Require Import JHFunctors1.
Require Import Aux2.


Class HFunctorP (I: Type) (F : fSt I) {Fun_F: HFunctor I F} :=
    {bHFlift :
        (forall i: I, F (HMFix I F) i -> Prop) -> 
           (forall i: I, F (HMFix I F) i -> Prop);
    
     hflift : forall i: I, F (HMFix I F) i -> Prop := 
                 DMFix I (F (HMFix I F)) bHFlift; 

     hflift_functor: DFunctor I (F (HMFix I F)) bHFlift;
     
     hflift_total: forall (i:I) (w: F (HMFix I F) i), hflift i w
    }.


Lemma hflifting1 (I: Type) (F: fSt I) {Fun_F: HFunctor I F} 
                 (Fun_FP: HFunctorP I F) 
                 (P: forall i:I, F (HMFix I F) i -> Prop): 
     (forall (i: I) (w: F (HMFix I F) i), P i w) -> 
     (forall (i: I) (w: F (HMFix I F) i), hflift i w -> P i w).
Proof.
auto.
Defined.


Lemma hflifting2 (I: Type) (F: fSt I) {Fun_F: HFunctor I F} 
                 (Fun_FP: HFunctorP I F) 
                 (P: forall i:I, F (HMFix I F) i -> Prop): 
     (forall (i: I) (w: F (HMFix I F) i), hflift i w -> P i w) ->
     (forall (i: I) (w: F (HMFix I F) i), P i w).
Proof.
intros.
assert (forall (i:I) (w: F (HMFix I F) i), hflift i w).
apply hflift_total.
auto.
Defined.


Lemma hflifting (I: Type) (F: fSt I) {Fun_F: HFunctor I F} 
                 (Fun_FP: HFunctorP I F) 
                 (P: forall i:I, F (HMFix I F) i -> Prop): 
     (forall (i: I) (w: F (HMFix I F) i), P i w) <-> 
     (forall (i: I) (w: F (HMFix I F) i), hflift i w -> P i w).
Proof.
constructor.
apply hflifting1. 
apply hflifting2.
Defined.



Class HFunctorSQU (I: Type) (F : fSt I) {Fun_F: HFunctor I F} :=
    { 
      hBiject1 : 
        forall (i: I) (x: F (HMFix I F) i), 
                    hmOut I F i (hmIn I F i x) = x; 

      hBiject2 : 
        forall (i: I) (x: HMFix I F i), 
                    hmIn I F i (hmOut I F i x) = x 
    }.

Class DFunctorSQU (I: Type) (K: I -> Type) (F: ffPr I K) 
                  {Fun_F: DFunctor I K F} :=
    { 
      dBiject1 : 
        forall (i: I) (w: K i) (x: F (DMFix I K F) i w), 
                    dmOut I K F i w (dmIn I K F i w x) = x; 

      dBiject2 : 
        forall (i: I) (w: K i) (x: DMFix I K F i w), 
                    dmIn I K F i w (dmOut I K F i w x) = x 
 
    }.



Lemma hliftR1 (K: Type) (F: fSt K) {Fun_F: HFunctor K F} (w: K)
              (P: F (HMFix K F) w -> Type) : 
       (forall x: F (HMFix K F) w, P x) -> 
                    (forall x: HMFix K F w, P (hmOut K F w x)).
Proof.
intros.
specialize (X (hmOut K F w x)).
assumption. 
Defined. 

Lemma hunliftR1 (K: Type) (F: fSt K) (w: K) (P: HMFix K F w -> Type): 
       (forall x: HMFix K F w, P x) -> 
                (forall x: F (HMFix K F) w, P (hmIn K F w x)).
Proof.
intros.
specialize (X (hmIn K F w x)).
assumption. 
Defined. 

Lemma hliftL1 (K: Type) (F: fSt K) 
              {Fun_F1: HFunctor K F} {Fun_F2: HFunctorSQU K F} (w: K)
             (P: F (HMFix K F) w -> Type) : 
       (forall x: HMFix K F w, P (hmOut K F w x)) -> 
                 (forall x: F (HMFix K F) w, P x).
Proof.
intros.
specialize (X (hmIn K F w x)).
rewrite <- hBiject1.
assumption. 
Defined. 

Lemma hunliftL1 (K: Type) (F: fSt K) 
                {Fun_F1: HFunctor K F} {Fun_F2: HFunctorSQU K F} (w: K)
               (P: HMFix K F w -> Type) : 
       (forall x: F (HMFix K F) w, P (hmIn K F w x)) ->  
                 (forall x: HMFix K F w, P x).
Proof.
intros.
specialize (X (hmOut K F w x)).
rewrite <- hBiject2.
assumption. 
Defined. 

Lemma mendlerHIndB (K: Type) (F: fSt K) 
       {Fun_F: HFunctor K F} {Fun_FP: HFunctorP K F}
       (P: forall w: K, F (HMFix K F) w -> Prop)   
       : DMAlgebra K (F (HMFix K F)) bHFlift P -> 
                               forall (w: K) (x: F (HMFix K F) w), P w x.
Proof. 
intro f.
apply (@hflifting2 K F Fun_F Fun_FP P).
intros i w H.
apply (dmfold K (F (HMFix K F)) bHFlift P f i w).
unfold hflift in H.
assumption. 
Defined.

Lemma mendlerHInd (K: Type) (F: fSt K) 
       {Fun_F: HFunctor K F} {Fun_FP: HFunctorP K F}
       {Fun_FX: HFunctorSQU K F} (P: forall w: K, HMFix K F w -> Prop)   
       : DMAlgebra K (F (HMFix K F)) bHFlift 
                        (fun w => compose (P w) (hmIn K F w)) -> 
                                    forall (w: K) (x: HMFix K F w), P w x.
Proof. 
intros.
apply (@hunliftL1 K F Fun_F Fun_FX w (P w)).
apply (@mendlerHIndB K F Fun_F Fun_FP (fun w => compose (P w) (hmIn K F w))).
assumption.
Defined.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)