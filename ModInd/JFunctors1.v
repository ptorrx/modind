
Require Import FunctionalExtensionality.

Require Import Aux2.

(* INDEXED ALGEBRAS for relations *)
(*
Definition dPr (K: Type) : Type := K -> Prop.

Definition fPr (K: Type) : Type := dPr K -> dPr K. 
*)

(* Church algebra - Alg^CI *) 
Definition JAlgebra (K: Type) 
  (F: fPr K) (A: dPr K) : Prop := forall (w:K), F A w -> A w.

(* Church fixpoint *)
Definition JFix (K: Type) 
   (F: fPr K) (w: K) : Prop := 
      forall (A: dPr K), JAlgebra K F A -> A w.


(* Mixin *)
Definition JMixin (K: Type) 
    (R: dPr K) (F: fPr K) (A: dPr K) :=
    (forall (w: K), R w -> A w) -> (forall (w: K), F R w -> A w).

(* Mendler algebra - Alg^MI *)
Definition JMAlgebra (K: Type) 
    (F: fPr K) (A: dPr K) : Prop :=
    forall (R: dPr K), JMixin K R F A.

(* Mendler fixpoint *)
Definition JMFix (K: Type) (F: fPr K) (w: K) : Prop :=
    forall (A: dPr K), JMAlgebra K F A -> A w.

(* Mendler fold *) Definition jmfold (K: Type) 
      (F: fPr K) : forall (A: dPr K) (f: JMAlgebra K F A), 
         forall (w: K), JMFix K F w -> A w := fun A f w e => e A f.


(* indexed functor class *)
Class JFunctor (K: Type) (F : fPr K) :=
    {jfmap :
      forall {A B : dPr K} (f : forall (w:K), A w -> B w), 
                                forall (w:K), F A w -> F B w;
      jfmap_fusion :
        forall (A B C: dPr K) (f : forall (w:K), A w -> B w) 
                              (g : forall (w:K), B w -> C w)
                              (w:K) (a: F A w),
          @jfmap B C g w (@jfmap A B f w a) = 
                  @jfmap A C (fun w => fun e => g w (f w e)) w a;
      jfmap_id :
        forall (A: dPr K) (w: K) (a: F A w),
          @jfmap A A (fun w => @id (A w)) w a = a
    }.


(* In morphism *)
Definition jmIn (K: Type) (F: fPr K) : 
        forall (w: K), F (JMFix K F) w -> JMFix K F w :=
    fun (w: K) (fe: F (JMFix K F) w) (A: dPr K) (f: JMAlgebra K F A) 
       => f (JMFix K F) (jmfold K F A f) w fe.

(* Out morphism, using Mendler fold *)
Definition jmOut (K: Type) (F : fPr K) {fun_F : JFunctor K F} (w: K)   
        (fe: JMFix K F w) : F (JMFix K F) w :=
                fe (F (JMFix K F)) 
                   (fun (R: dPr K) 
                        (rec: forall w0, R w0 -> (F (JMFix K F)) w0) 
                        (w: K) (fa: F R w) =>
                  (@jfmap K F fun_F R (JMFix K F) 
                       (fun w0 (x:R w0) => jmIn K F w0 (rec w0 x)) w fa)).

(* Church fold *)
Definition jfold (K: Type) 
  (F: fPr K) (fun_F: JFunctor K F):
    forall (A: dPr K) (f: JAlgebra K F A), 
         forall (w: K), JMFix K F w -> A w :=
      fun (A: dPr K) (f: JAlgebra K F A) (w: K) (e: JMFix K F w) 
              => jmfold K F A 
                   (fun (R: dPr K) 
                        (rec: forall (w: K), R w -> A w) 
                        (w: K)
                        (fa: F R w) => f w (@jfmap K F fun_F R A rec w fa)) w e.


(* Out morphism, using Church fold *)
Definition jmOut_C (K: Type) 
     (F : fPr K) (fun_F : JFunctor K F) : 
        forall (w: K), JMFix K F w -> F (JMFix K F) w :=
    jfold K F fun_F (F (JMFix K F)) 
               (@jfmap K F fun_F (F (JMFix K F)) (JMFix K F) (jmIn K F)).


 (* turns a Church fixpoint into a Mendler one *)
  Definition tJC2M (K: Type) (F: fPr K) (w: K) (x: JFix K F w) : 
                                                   JMFix K F w :=
    fun (C: dPr K) 
        (f: forall A: dPr K, (forall w: K, A w -> C w) -> 
                             (forall w: K, F A w -> C w)) => 
                 x C (f C (fun w => @id (C w))).

 (* turns a Mendler fixpoint into a Church one *)
  Definition tJM2C (K: Type) (F: fPr K) {fun_F: JFunctor K F} (w: K)
                   (x: JMFix K F w) : JFix K F w :=
    fun (C: dPr K) (f: forall w: K, F C w -> C w) =>  
           x C (fun (A: dPr K) (r: forall w: K, A w -> C w) w y => 
                                    f w (@jfmap K F fun_F A C r w y)). 



(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)