
Require Export Basics.


(** decidable equality class *)

Class DEq (V:Set) : Type := {
   dEq : forall x y: V, {x = y} + {x <> y}
}.  


(** general environment type *)

Definition genEnv (T U: Set) : Set := T -> option U.

Definition envMap (T U1 U2: Set) (f: U1 -> U2) (g: genEnv T U1) (x:T) : 
    option U2 := match g x with 
     | Some y => Some (f y)
     | None => None
     end.

Inductive findE (T U: Set) (m: genEnv T U) (k: T) : U -> Prop := 
  | FindE : forall x: U, m k = Some x -> findE T U m k x.

Definition emptyE (T U: Set): genEnv T U := fun (x:T) => None.

Definition overrideE (T U: Set)  
    (f1 f2: genEnv T U) 
    (x: T) : option U :=
  match f1 x with
  | Some y => Some y
  | None => f2 x
  end. 


Inductive overrideEP (T U: Set)   
    (f1 f2 f3: genEnv T U) : Prop :=
   | OverrideEP : f3 = overrideE T U f1 f2 -> overrideEP T U f1 f2 f3.


Definition updateE (T U: Set) (h: DEq T) (g: genEnv T U) (z: T) (t: U) 
      : genEnv T U :=
  fun (w:T) => match (dEq w z) with
  | left _ => Some t
  | right _ => g w
  end.

Definition singletonE (T U: Set) (h: DEq T) (z: T) (t: U) : 
   genEnv T U := 
       updateE T U h (emptyE T U) z t. 

Inductive disjoint (T U: Set) (m1 m2: genEnv T U) : Prop :=
   disjoint1 : (forall x:T, or (m1 x = None) (m2 x = None)) -> 
                   disjoint T U m1 m2.

Inductive disjointH (T U1 U2: Set) (m1: genEnv T U1) 
    (m2: genEnv T U2) : Prop :=
   disjointH1 : (forall x:T, or (m1 x = None) (m2 x = None)) -> 
                   disjointH T U1 U2 m1 m2.


Inductive includeEnv (T U: Set) (m1 m2: genEnv T U) : Prop :=
   includeEnv1 : (forall x:T, or (m1 x = None) (m1 x = m2 x)) -> 
                   includeEnv T U m1 m2.



(** lifting for option types *)

Inductive liftEq (T:Type) : (option T) -> T -> Prop := 
   | LiftEq : forall e:T, liftEq T (Some e) e.

Inductive liftWkEq (T:Type) : (option T) -> T -> Prop := 
   | LiftWkEq1 : forall e:T, liftWkEq T (Some e) e
   | LiftWkEq2 : forall e:T, liftWkEq T None e.

Definition liftM (X Y:Type) (f: X -> Y) (x:option X) : option Y :=
   match x with
   | Some x1 => Some (f x1)
   | None => None
   end.

Definition lift2M (X Y Z:Type) (f: X -> Y -> Z) (x:option X) (y:option Y) : 
 option Z :=
   match x,y with
   | Some x1, Some y1 => Some (f x1 y1)
   | _,None => None
   | None,_ => None
   end.

Definition liftH (X Y: Set) (f: genEnv X Y) (x:option X) : option Y :=
   match x with
   | Some x1 => f x1
   | None => None
   end.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)