Require Import FunctionalExtensionality.

Require Import Functors1.

(* Require Import Functors2. *)
Require Export Aux1.


(** general auxiliary definitions *)

Definition dSt (K: Type) : Type := K -> Set.

Definition fSt (K: Type) : Type := dSt K -> dSt K. 

Definition dPr (K: Type) : Type := K -> Prop.

Definition fPr (K: Type) : Type := dPr K -> dPr K. 

Definition ddPr (I: Type) (K: I -> Type) : Type := 
                           forall (i: I), K i -> Prop.

Definition ffPr (I: Type) (K: I -> Type) : Type := 
                           ddPr I K -> forall (i: I), K i -> Prop. 

(***********)

Definition dSet (K: Type) : Type := K -> Set.

Definition fSet (K: Type) : Type := dSet K -> dSet K. 

Definition tSet (K: Type) : Type := dSet K -> dSet K -> dSet K. 

Definition dProd (K: Type) : Type := prod (dSet K) (dSet K).

Definition tPr (K: Type) : Type := dPr K -> dPr K -> dPr K. 

Definition pProd (K: Type) : Type := prod (dPr K) (dPr K).

Definition abSet : Type := Set -> Set-> Set. 


(* DEFINITIONS *)

Definition uncurry2G (T A B: Type) (f: A -> B -> T) 
      (r: prod A B) : T :=
   f (fst r) (snd r).

Definition curry2G (T A B: Type) (f: (prod A B) -> T) 
       (x: A) (y: B) : T := f (pair x y).


Definition uncurry2 {A B: Type} (f: A -> B -> Set) 
      (r: prod A B) : Set := uncurry2G Set A B f r.

Definition curry2 {A B: Type} (f: (prod A B) -> Set) 
       (x: A) (y: B) : Set := curry2G Set A B f x y.


Definition uncurry2P {A B: Type} (f: A -> B -> Prop) 
      (r: prod A B) : Prop := uncurry2G Prop A B f r.

Definition curry2P {A B: Type} (f: (prod A B) -> Prop) 
       (x: A) (y: B) : Prop := curry2G Prop A B f x y.


Definition deepUncurryG (T A B: Type) (f: (A -> B -> T) -> (A -> B -> T)) 
  (x: prod A B -> T) : (prod A B -> T) :=
   uncurry2G T A B (f (curry2G T A B x)).

Definition deepCurryG (T A B: Type) (f: (prod A B -> T) -> (prod A B -> T)) 
  (x: A -> B -> T) : (A -> B -> T) :=
   curry2G T A B (f (uncurry2G T A B x)).

Definition deepUncurry {A B: Type} (f: (A -> B -> Set) -> (A -> B -> Set)) 
  (x: prod A B -> Set) : (prod A B -> Set) :=
   deepUncurryG Set A B f x.

Definition deepCurry {A B: Type} (f: (prod A B -> Set) -> (prod A B -> Set)) 
  (x: A -> B -> Set) : (A -> B -> Set) :=
   deepCurryG Set A B f x.

Definition deepUncurryP {A B: Type} (f: (A -> B -> Prop) -> (A -> B -> Prop)) 
  (x: prod A B -> Prop) : (prod A B -> Prop) :=
   deepUncurryG Prop A B f x.

Definition deepCurryP {A B: Type} (f: (prod A B -> Prop) -> (prod A B -> Prop)) 
  (x: A -> B -> Prop) : (A -> B -> Prop) :=
   deepCurryG Prop A B f x.



(* more definitions *)

Definition quad (T1 T2 T3 T4: Type) : Type :=
    prod (prod T1 T2) (prod T3 T4).

Definition quad1 {T1 T2 T3 T4: Type} (x: quad T1 T2 T3 T4) : T1 :=
   fst (fst x).  

Definition quad2 {T1 T2 T3 T4: Type} (x: quad T1 T2 T3 T4) : T2 :=
   snd (fst x).  

Definition quad3 {T1 T2 T3 T4: Type} (x: quad T1 T2 T3 T4) : T3 :=
   fst (snd x).  

Definition quad4 {T1 T2 T3 T4: Type} (x: quad T1 T2 T3 T4) : T4 :=
   snd (snd x).  

(**)

Definition uncurry4G (T A B C D: Type) (f: A -> B -> C -> D -> T) 
      (r: quad A B C D) : T :=
   f (quad1 r) (quad2 r) (quad3 r) (quad4 r).

Definition curry4G (T A B C D: Type) (f: (quad A B C D) -> T) 
       (x: A) (y: B) (w: C) (z: D): T := f (pair (pair x y) (pair w z)).

Definition uncurry4 {A B C D: Type} : 
       (A -> B -> C -> D -> Prop) -> quad A B C D -> Prop :=
   uncurry4G Prop A B C D.
       
Definition curry4 {A B C D: Type} : 
        (quad A B C D -> Prop) -> (A -> B -> C -> D -> Prop) := 
   curry4G Prop A B C D.

Definition deepurry4 {A B C D: Type} : 
        (quad A B C D -> Prop) -> (A -> B -> C -> D -> Prop) := 
   curry4G Prop A B C D.

Definition deepUncurry4G (T A B C D: Type) 
 (f: (A -> B -> C -> D -> T) -> (A -> B -> C -> D -> T)) 
  (x: quad A B C D -> T) : (quad A B C D -> T) :=
   uncurry4G T A B C D (f (curry4G T A B C D x)).

Definition deepCurry4G (T A B C D: Type) 
 (f: (quad A B C D -> T) -> (quad A B C D -> T)) 
  (x: A -> B -> C -> D -> T) : (A -> B -> C -> D -> T) :=
   curry4G T A B C D (f (uncurry4G T A B C D x)).

Definition deepUncurry4 {A B C D: Type} 
 (f: (A -> B -> C -> D -> Set) -> (A -> B -> C -> D -> Set)) 
  (x: quad A B C D -> Set) : (quad A B C D -> Set) :=
   deepUncurry4G Set A B C D f x.

Definition deepCurry4 {A B C D: Type} 
 (f: (quad A B C D -> Set) -> (quad A B C D -> Set)) 
  (x: A -> B -> C -> D -> Set) : (A -> B -> C -> D -> Set) :=
   deepCurry4G Set A B C D f x.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)