(* Paolo Torrini, KU Leuven *)

Require Import FunctionalExtensionality.

Require Import Functors1.
Require Import Functors2.
Require Import JFunctors1.
Require Export Aux1.
Require Export Aux2.
Require Import Functors3.


Variable DVr : Set.
Variable TVr : Set.

Parameter DVrEq : forall (x y : DVr), {x = y} + {x <> y}.
Parameter TVrEq : forall (x y : TVr), {x = y} + {x <> y}.

Instance deqDVr: DEq DVr :=
{
  dEq := DVrEq
}.

Instance deqTVr: DEq TVr :=
{
  dEq := TVrEq
}.


(* TYPES *)

Inductive bTyp (T: Set)
    : Set := 
  | TId (v: TVr)
  | TFun (t1 t2: T)
  | TBot.

      
Definition rTyp : Set := MFix bTyp.

Definition bTypT : Set := bTyp rTyp.

Definition typEnv : Set := genEnv DVr rTyp.


(**)

Definition TTyp (x: bTyp rTyp) : rTyp := mIn bTyp x.

Definition TIdT (v: TVr) : bTypT := TId rTyp v.

Definition TFunT (t1 t2: rTyp) : bTypT := TFun rTyp t1 t2.

Definition TBotT : bTypT := TBot rTyp.



(* TYPE ENVIRONMENTS *)

Definition findTE (env: typEnv) (x: DVr) : rTyp -> Prop :=
           findE DVr rTyp env x.

Definition emptyTE : typEnv := emptyE DVr rTyp.

Definition overrideTE (f1 f2: typEnv) (x: DVr) : 
                      option rTyp := 
           overrideE DVr rTyp f1 f2 x. 

Definition updateTE (f: typEnv) (x: DVr) (v: rTyp) : 
         typEnv := updateE DVr rTyp deqDVr f x v.

Definition disjointTE  
     (m1 m2: DVr -> option rTyp) : Prop :=
   disjoint DVr rTyp m1 m2.



(* TYPE FUNCTOR *)

Definition bTyp_fmap (B C : Set) (f : B -> C) 
    (Aa : bTyp B) : bTyp C :=
   match Aa with 
     | TId v => TId _ v
     | TFun t1 t2 => TFun _ (f t1) (f t2)
     | TBot => TBot _
   end.

                         
Global Instance bTyp_Functor : 
   Functor bTyp :=
  {| fmap := bTyp_fmap |}.  
destruct a.
reflexivity.
reflexivity.
reflexivity.
destruct a.
reflexivity.
reflexivity.
reflexivity.
Defined.


Definition mOut_Typ : rTyp -> bTyp rTyp :=
  mOut bTyp.

(******************************)


(* characteristic predicate functor *)
Inductive bIsTyp (T: rTyp -> Prop)
    : rTyp -> Prop := 
  | TIdIsTyp : forall v: TVr, 
            bIsTyp T (TTyp (TIdT v)) 
  | TFunIsTyp : forall (t1 t2: rTyp),
            T t1 ->
            T t2 -> 
            bIsTyp T (TTyp (TFunT t1 t2)) 
  | TBotIsTyp : bIsTyp T (TTyp TBotT).


Definition auxT (V U Z: Set) 
       (k: Z -> U)
       (g: genEnv V U) 
       (B C: Z -> Prop) 
       (f: forall t0: Z, B t0 -> C t0) 
       (prem: forall (v: V) 
                     (t: Z), 
                     findE V U g v (k t) -> B t)
       (v:V) 
       (t: Z) 
       (pr1: findE V U g v (k t)) : C t  :=
  f t (prem v t pr1). 


Definition bIsTyp_jfmap  
       (B C: rTyp -> Prop)
       (f: forall  
           (d0: rTyp), B d0 -> C d0) 
       (d: rTyp)
       (Aa: bIsTyp B d) :
            bIsTyp C d :=
   match Aa in (bIsTyp _ d5) 
            return (bIsTyp C d5) with 
    | TIdIsTyp v =>      
         TIdIsTyp C v  
    | TFunIsTyp t1 t2 prem1 prem2 =>
         TFunIsTyp C t1 t2 (f t1 prem1) (f t2 prem2) 
    | TBotIsTyp => TBotIsTyp C
  end.


Definition rIsTyp : rTyp -> Prop := JMFix rTyp bIsTyp.

Definition bIsTypX : rTyp -> Prop :=  bIsTyp rIsTyp.


Global Instance bIsTyp_JFunctor : 
   JFunctor rTyp bIsTyp :=
 {| jfmap := bIsTyp_jfmap |}.
intros A B C.
intros f g w.
intro a.
destruct a.
reflexivity.
reflexivity.
reflexivity.
destruct a.
reflexivity.
reflexivity.
reflexivity.
Defined.



(***************************************************)


Definition btSubst (P: rTyp -> Type)
    : forall t1 t2: rTyp, t1 = t2 -> P t1 -> P t2.
Proof.
intros.
rewrite <- H.
assumption.
Defined.


Definition btSubst2 (x1 x2: bTypT) (y1 y2: rTyp) 
                    (p1: TTyp x1 = y1) (p2: TTyp x2 = y2)
                    (P: rTyp -> rTyp -> Type) 
                    (w: P (TTyp x1) (TTyp x2)) : P y1 y2.
Proof.
rewrite <- p1.
rewrite <- p2.
assumption.
Defined.    



Definition btSubstX2 (A B: Type) (x1 x2 y1 y2: A) (p q: A -> B)
                    (p1: x1 = y1) (p2: x2 = y2) (p3: p = q) 
                    (P: B -> B -> Type)
                    (w: P (q y1) (q y2)) : P (p x1) (p x2).
Proof.
rewrite p3.
rewrite p2.
rewrite p1.
assumption.
Defined.    


Definition btSubstX1 (A B: Type) (x y: A) (p q: A -> B)
                    (p1: x = y) (p2: p = q) 
                    (P: B -> Type)
                    (w: P (q y)) : P (p x).
Proof.
rewrite p2.
rewrite p1.
assumption.
Defined.    



Definition btSubstY2 (A : Set)  
                    (fin: bTyp A -> A)
                    (fout: A -> bTyp A)
                    (x1 x2: bTyp A) (y1 y2: A) 
                    (p1: y1 = fin x1) (p2: y2 = fin x2)
                    (P: A -> A -> Type) 
                    (w: P (fin x1) (fin x2)) : P y1 y2.
Proof.
rewrite p1.
rewrite p2.
assumption.
Defined.    


Definition btSubstA1 (A B: Type) (x: A) (p q: A -> B)
                    (k: p = q) 
                    (P: B -> Type)
                    (w: P (q x)) : P (p x).
Proof.
rewrite k.
assumption.
Defined.    


Definition btSubstB1 (A: Type) (x: A) (p: A -> A)
                    (k: x = p x) 
                    (P: A -> Type)
                    (w: P (p x)) : P x.
Proof.
rewrite k.
assumption.
Defined.    


(* predicatisation sigma-algebra *)
Definition xbGoForth7 (* Fun_F: @FunctorSQU bTyp bTyp_Functor) *)
                    (A : Set)  
                    (rec: A -> sig rIsTyp) 
                    (t: bTyp A) : sig rIsTyp := 
      match t in bTyp _ return sig rIsTyp with 
  | TId v => @exist rTyp rIsTyp 
                  (TTyp (TIdT v))   
                  (jmIn rTyp bIsTyp (TTyp (TIdT v)) 
                        (TIdIsTyp rIsTyp v)) 
  | TFun t1 t2 => @exist rTyp rIsTyp _ (* (TFun A t1 t2) *)
                (jmIn rTyp bIsTyp _ (* TFun A t1 t2 *) 
                   (TFunIsTyp rIsTyp
                          (proj1_sig (rec t1)) (proj1_sig (rec t2)) 
                          (proj2_sig (rec t1))
                          (proj2_sig (rec t2))
                   )
                )
  | TBot => @exist rTyp rIsTyp 
            (TTyp TBotT)
            (jmIn rTyp bIsTyp _ 
                  (TBotIsTyp rIsTyp))
  end.


Definition xbGoForthF7 (rec: rTyp -> sig rIsTyp) 
                    (t: bTypT) : sig rIsTyp := xbGoForth7 rTyp rec t.

Definition xrGoForthF7 (t: rTyp) 
                     : sig rIsTyp := 
                         @mfold bTyp (sig rIsTyp) xbGoForth7 t.

Definition goBack (w: sig rIsTyp) : rTyp := proj1_sig w.




Inductive bTypEq (T: (rTyp * rTyp) -> Prop)
    : (rTyp * rTyp) -> Prop := 
  | TIdTypEq : forall v: TVr, 
            bTypEq T (TTyp (TIdT v), TTyp (TIdT v)) 
  | TFunTypEq : forall (t1 t2 t3 t4: rTyp), 
            T (t1, t3) -> 
            T (t2, t4) -> 
            bTypEq T (TTyp (TFunT t1 t2), TTyp (TFunT t3 t4)) 
  | TBotTypEq : bTypEq T (TTyp TBotT, TTyp TBotT).




Definition rTypEq : (rTyp * rTyp) -> Prop := JMFix (rTyp * rTyp) bTypEq.

Definition bTypEqX : (rTyp * rTyp) -> Prop :=  bTypEq rTypEq.

Definition bTypEqY : (rTyp * rTyp) -> Prop :=  
                     bTypEq (fun x => (fst x) = (snd x)).


(*******************************)

Lemma wfInd : forall (e: bTyp (sig rIsTyp)),  
                      proj1_sig (xbGoForth7 (sig rIsTyp) id e) = 
      mIn bTyp (@fmap bTyp bTyp_Functor (sig rIsTyp) rTyp 
                                        (@proj1_sig rTyp rIsTyp) e).
Proof. 
intros.
destruct e.
reflexivity.
reflexivity.
reflexivity.
Defined. 


Lemma xbGoForth7_MA_inst : 
    forall (A: Set) (e: bTyp A) (rec: A -> sig rIsTyp),
              xbGoForth7 A rec e = xbGoForth7 (sig rIsTyp) id (fmap rec e). 
Proof.
intros.
destruct e.
reflexivity.
reflexivity.
reflexivity.
Defined.



(*************************)


Global Instance xbGoForth7_MendlerAlgebra : 
        MendlerAlgebra (sig rIsTyp) xbGoForth7 :=
{| ME_1 := xbGoForth7_MA_inst
|}.


Global Instance xbGoForth7_MPAlgebra : 
        MPAlgebra bTyp rIsTyp xbGoForth7 :=
{| WF_MInd := wfInd
|}.


Lemma rIsTyp_total : forall (x : rTyp)
      (fUP' : Universal_Property'_mfold x), rIsTyp x.
Proof.
apply (@Ind bTyp bTyp_Functor rIsTyp xbGoForth7 xbGoForth7_MPAlgebra xbGoForth7_MendlerAlgebra).
Defined.

(***********************************************)

Definition isoreq (w: rTyp) : Prop :=
  (compose (mIn bTyp) (mfold bTypT (fun (A: Set) 
                                   (r: A -> bTypT) 
                                   (a: bTyp A) => 
                                       fmap (compose (mIn bTyp) r) a))) w = w.

Lemma rTypIsoRec1 : JMAlgebra rTyp bIsTyp isoreq.
Proof.
unfold JMAlgebra.
unfold JMixin.
intros.
inversion H0; subst.
unfold isoreq.
reflexivity.
Focus 2.
unfold isoreq.
reflexivity.
apply H in H1.
apply H in H2.
unfold isoreq.
simpl.
unfold isoreq in H1.
unfold isoreq in H2.
unfold compose.
unfold mfold.
unfold mIn.
unfold TTyp.
unfold mIn.
unfold mfold.
eapply (@functional_extensionality_dep Set).
intros.
eapply functional_extensionality_dep.
intros.
rewrite <- H1 at 2.
rewrite <- H2 at 2.
reflexivity.
Defined.


Instance bTyp_FunctorP1 (p: FLiftTotal bTyp rIsTyp)
             : @FunctorP bTyp bTyp_Functor :=
   {|  
       bFlift := bIsTyp;
       flift_functor := bIsTyp_JFunctor;
       flift_total := p
   |}.


Lemma rTypIsoRec2 (p: FLiftTotal bTyp rIsTyp): forall w: rTyp, isoreq w.
Proof.
apply (@mendlerInd bTyp bTyp_Functor (bTyp_FunctorP1 p) isoreq).
apply rTypIsoRec1.
Defined.

Lemma rTyp_biject2 (p: FLiftTotal bTyp rIsTyp): 
                     forall x: rTyp, mIn bTyp (mOut bTyp x) = x.
Proof.
intros.
apply rTypIsoRec2.
assumption.
Defined.

Lemma rTyp_biject1 (p: FLiftTotal bTyp rIsTyp): 
                     forall x: bTypT, mOut bTyp (mIn bTyp x) = x.
Proof.
apply (biject1lemmaP bTyp (rTyp_biject2 p)).
Defined.




(*
Definition isoreqI (w: bTypT) : Prop :=
  (compose (mfold bTypT (fun (A: Set) 
                             (r: A -> bTypT) 
                             (a: bTyp A) => 
                   fmap (compose (mIn bTyp) r) a)) (mIn bTyp)) w = w.
*)

(********************************)

Definition uprop1 (w: rTyp) : Prop := forall (B : Set) 
      (f : MAlgebra bTyp B) (h : rTyp -> B),
        (forall e: bTypT, h (TTyp e) = f rTyp h e) ->  
                                     h w = mfold B f w. 

(*
Fixpoint ffix (B : Set) (f : MAlgebra bTyp B) : rTyp -> B :=
     compose (f rTyp (ffix B f)) (mOut bTyp).
*)

Definition uprop2 (w: rTyp) : Prop := forall (B : Set) 
      (f : MAlgebra bTyp B) (h : rTyp -> B),
        (h = compose (f rTyp h) (mOut bTyp)) ->  
                                     h w = mfold B f w. 

(*
Definition uprop3 (w: rTyp) : forall (B : Set) 
      (f : Algebra F B) (h : Fix F -> B),
        (forall e,  h (in_t e) = f (fmap h e)) ->
                            h x = fold_ _ f x.
*)

Lemma rTypUProp : JMAlgebra rTyp bIsTyp uprop2.
Proof.
unfold JMAlgebra.
unfold JMixin.
intros.
inversion H0; subst.
unfold uprop2.
intros.
unfold mfold.
rewrite H1.
unfold compose.
unfold TTyp at 2.
unfold mIn.

  (compose (mIn bTyp) (mfold bTypT (fun (A: Set) 
                                   (r: A -> bTypT) 
                                   (a: bTyp A) => 
                                       fmap (compose (mIn bTyp) r) a))) w = w.


(*******************************)


(* class MFoldUniversalProperty for Mendler encodings *)
(*
Class Universal_Property'_mfold {F} {fun_F : Functor F} (x : F (MFix F)) :=
    { ME_fUP' : forall (B : Set) (f : MAlgebra F B) (h : MFix F -> B),
        (compose h (mIn F) = f (MFix F) h) -> (h x = x f)  
    }.

        (compose h (mIn F) = f (MFix F) h) -> 
                              f (MFix F) h x = f (MFix F) (mfold B f) x 

mfold B f = fun x => x B f 
*)

(*
assert (
      (bTyp_fmap (MFix bTyp) (MFix bTyp)
         (fun (x : MFix bTyp) (A0 : Set) (f0 : MAlgebra bTyp A0) =>
          f0 (MFix bTyp) (fun e : MFix bTyp => e A0 f0)
            (x bTypT
               (fun (A1 : Set) (r : A1 -> bTypT) (a : bTyp A1) =>
                bTyp_fmap A1 (MFix bTyp)
                  (fun (x0 : A1) (A2 : Set) (f1 : MAlgebra bTyp A2) =>
                   f1 (MFix bTyp) (fun e : MFix bTyp => e A2 f1) (r x0)) a)))
         (TFunT t1 t2)) =
  (TFunT t1 t2)
).
assert (
      (bTyp_fmap (MFix bTyp) (MFix bTyp)
         (fun (x : MFix bTyp) (A0 : Set) (f0 : MAlgebra bTyp A0) =>
          f0 (MFix bTyp) (fun e : MFix bTyp => e A0 f0)
            (x bTypT
               (fun (A1 : Set) (r : A1 -> bTypT) (a : bTyp A1) =>
                bTyp_fmap A1 (MFix bTyp)
                  (fun (x0 : A1) (A2 : Set) (f1 : MAlgebra bTyp A2) =>
                   f1 (MFix bTyp) (fun e : MFix bTyp => e A2 f1) (r x0)) a)))
         (TFunT t1 t2)) =
  (TFunT (compose (mIn bTyp)
         (mfold bTypT
            (fun (A : Set) (r : A -> bTypT) (a : bTyp A) =>
             fmap (compose (mIn bTyp) r) a)) t1) (compose (mIn bTyp)
         (mfold bTypT
            (fun (A : Set) (r : A -> bTypT) (a : bTyp A) =>
             fmap (compose (mIn bTyp) r) a)) t2))
).
reflexivity.
apply functional_extensionality.
*)

(*
Lemma rTypIsoRec2 : forall w: rTyp,  
  (compose (mIn bTyp) (mfold bTypT (fun (A: Set) 
                                   (r: A -> bTypT) 
                                   (a: bTyp A) => 
                                       fmap (compose (mIn bTyp) r) a))) w = w.
Proof.


Lemma rTypIsoRec3 : forall w: bTypT,  
   fmap (compose (mIn bTyp) (mfold bTypT (fun (A: Set) 
                                   (r: A -> bTypT) 
                                   (a: bTyp A) => 
                                       fmap (compose (mIn bTyp) r) a))) w = w.
Proof.
intros.
destruct w.
reflexivity.
Focus 2.
reflexivity.
*)


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)

