(* Paolo Torrini, KU Leuven *)

Require Import Functors1.
Require Import JFunctors1.
Require Import Aux2.

(* general infrastructure to support the use of modular induction  *)

(*
classes: Predicatisable
         IsoEquations
         IsoEquations^I
*)

Definition FLiftTotal (F : Set -> Set) (P: Fix F -> Prop): Prop :=  
           forall w: Fix F, P w. 

(* class Predicatisable *)
Class FunctorP (F : Set -> Set) {Fun_F: Functor F} :=
    {(* char_pred_sig *)
     bFlift :
        (Fix F -> Prop) -> (Fix F -> Prop);

     (* char_pred *)
     flift : Fix F -> Prop := JMFix (Fix F) bFlift; 

     (* char_pred_sig_functor *)
     flift_functor: JFunctor (Fix F) bFlift;

     (* total_pred *)
     flift_total: FLiftTotal F flift 
    }.


Lemma flifting1 (F: Set -> Set) (Fun_F: Functor F) (Fun_FP: FunctorP F) 
               (P: Fix F -> Prop): 
     (forall w: Fix F, P w) -> (forall w: Fix F, flift w -> P w).
Proof.
auto.
Defined.

Lemma flifting2 (F: Set -> Set) (Fun_F: Functor F) (Fun_FP: FunctorP F) 
               (P: Fix F -> Prop): 
     (forall w: Fix F, flift w -> P w) -> (forall w: Fix F, P w).
Proof.
intros.
assert (forall w: Fix F, flift w).
apply flift_total.
auto.
Defined.


(* induction switch lemma: induct_switch *)
Lemma flifting (F: Set -> Set) (Fun_F: Functor F) (Fun_FP: FunctorP F) 
               (P: Fix F -> Prop): 
     (forall w: Fix F, P w) <-> (forall w: Fix F, flift w -> P w).
Proof.
constructor.
apply flifting1. 
apply flifting2.
Defined.


(* class IsoEquations *)
Class FunctorSQU (F : Set -> Set) {Fun_F: Functor F} :=
    { 
      biject1 : 
        forall x: F (Fix F), mOut F (mIn F x) = x; 

      biject2 : 
        forall x: Fix F, mIn F (mOut F x) = x 
    }.

(* class IsoEquations^I *)
Class JFunctorSQU (K: Type) (F: fPr K) {Fun_F: JFunctor K F} :=
    { 
      jBiject1 : 
        forall (w: K) (x: F (JMFix K F) w), 
                    jmOut K F w (jmIn K F w x) = x; 

      jBiject2 : 
        forall (w: K) (x: JMFix K F w), 
                    jmIn K F w (jmOut K F w x) = x 
 
    }.



Lemma liftR1 (F: Set -> Set) {Fun_F: Functor F} (P: F (Fix F) -> Type) : 
       (forall x: F (Fix F), P x) -> (forall x: Fix F, P (mOut F x)).
Proof.
intros.
specialize (X (mOut F x)).
assumption. 
Defined. 

Lemma unliftR1 (F: Set -> Set) (P: Fix F -> Type) : 
       (forall x: Fix F, P x) -> (forall x: F (Fix F), P (mIn F x)).
Proof.
intros.
specialize (X (mIn F x)).
assumption. 
Defined. 

Lemma liftL1 (F: Set -> Set) {Fun_F1: Functor F} {Fun_F2: FunctorSQU F} 
             (P: F (Fix F) -> Type) : 
       (forall x: Fix F, P (mOut F x)) -> (forall x: F (Fix F), P x).
Proof.
intros.
specialize (X (mIn F x)).
rewrite <- biject1.
assumption. 
Defined. 

Lemma unliftL1 (F: Set -> Set) {Fun_F1: Functor F} {Fun_F2: FunctorSQU F} 
               (P: Fix F -> Type) : 
       (forall x: F (Fix F), P (mIn F x)) -> (forall x: Fix F, P x).
Proof.
intros.
specialize (X (mOut F x)).
rewrite <- biject2.
assumption. 
Defined. 

(* Mendler induction lemma: Mendler_induct *)
Lemma mendlerInd (F: Set -> Set) {Fun_F: Functor F} {Fun_FP: FunctorP F}
       (P: Fix F -> Prop)   
       : JMAlgebra (Fix F) bFlift P -> forall w: Fix F, P w.
Proof. 
intro f.
apply (flifting2 F Fun_F Fun_FP P). 
intros w H.
apply (jmfold (Fix F) bFlift P f w).
unfold flift in H.
assumption. 
Defined.

Lemma mendlerIndB (F: Set -> Set) {Fun_F: Functor F} {Fun_FP: FunctorP F}
       {Fun_FX: FunctorSQU F} (P: F (Fix F) -> Prop)   
       : JMAlgebra (Fix F) bFlift (compose P (mOut F)) -> 
                                                  forall w: F (Fix F), P w.
Proof. 
intros.
apply (@liftL1 F Fun_F Fun_FX P).
apply (@mendlerInd F Fun_F Fun_FP (compose P (mOut F))).
assumption.
Defined.


Lemma mInInvertEq (F: Set -> Set) {Fun_F1: Functor F} 
             {Fun_F2: FunctorSQU F} (x y: F (MFix F)) : 
                    mIn F x = mIn F y -> x = y. 
Proof.
intros.
assert (mOut F (mIn F x) = mOut F (mIn F y)).
intros.
rewrite H.
reflexivity.
rewrite biject1 in H0.
rewrite biject1 in H0.
assumption. 
Defined.

Lemma mOutInvertEq (F: Set -> Set) {Fun_F1: Functor F} 
             {Fun_F2: FunctorSQU F} (x y: MFix F) : 
                    mOut F x = mOut F y -> x = y. 
Proof.
intros.
assert (mIn F (mOut F x) = mIn F (mOut F y)).
intros.
rewrite H.
reflexivity.
rewrite biject2 in H0.
rewrite biject2 in H0.
assumption. 
Defined.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)