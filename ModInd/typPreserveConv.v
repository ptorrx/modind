(* Paolo Torrini, KU Leuven *)

Require Export Aux1.

(* example of type preservation proof based on conventional datatypes *)

Variable DVr : Set.
Variable TVr : Set.

Parameter DVrEq : forall (x y : DVr), {x = y} + {x <> y}.
Parameter TVrEq : forall (x y : TVr), {x = y} + {x <> y}.

Instance deqDVr: DEq DVr :=
{
  dEq := DVrEq
}.

Instance deqTVr: DEq TVr :=
{
  dEq := TVrEq
}.


(* types *)
Inductive TTyp 
    : Set := 
  | TTId (v: TVr)
  | TTFun (t1 t2: TTyp)
  | TTEnv (h: DVr -> option TTyp)
  | TBot.


Variable NN: TVr.

(* represents the type of naturals - Nat *)
Definition TNN : TTyp := TTId NN.


(* expressions -Exp *)
Inductive rNum 
    : Set := 
  | NLit (v: nat)
  | NSum (e1 e2: rNum)
  | NErr.


(* values *)
Inductive VVal : Set := VLit (v: nat).

Definition VSum (v1 v2: option VVal) : option VVal :=
  match v1 with 
  | Some (VLit n1) => match v2 with 
              | Some (VLit n2) => Some (VLit (n1 + n2))
              | _ => None
              end
  | _ => None
  end.


Definition val2natopt (v: VVal) : option nat :=
  match v with 
  | VLit n => Some n 
end.


(* evaluation *)
Fixpoint rEval (e: rNum) : option VVal :=
  match e with
  | NLit n => Some (VLit n)
  | NSum e1 e2 => VSum (rEval e1) (rEval e2)
  | NErr => None
  end. 


(* typing relation *)
Inductive rTyping : (rNum * TTyp) -> Prop :=
  | NLitTyp : forall n: nat, rTyping (NLit n, TNN)
  | NSumTyp : forall (e1 e2: rNum), 
        rTyping (e1, TNN) -> rTyping (e2, TNN) -> rTyping (NSum e1 e2, TNN)
  | NErrTyp : rTyping (NErr, TBot).


Definition val2num (v: option VVal) : rNum :=
  match v with 
  | Some (VLit n) => NLit n 
  | _ => NErr
  end.


(* type preservation property - type_preservation *)
Definition typPreserve (e: rNum) : Prop :=
    forall t: TTyp, rTyping (e, t) -> rTyping (val2num (rEval e), t).


(* auxiliary lemma - sum_lemma *)
Lemma sumTypLemma (e1 e2: rNum): 
         rTyping (val2num (rEval e1), TNN) -> 
         rTyping (val2num (rEval e2), TNN)
             -> rTyping (val2num (VSum (rEval e1) (rEval e2)), TNN).
Proof. 
intros.
destruct (rEval e1).
destruct v.
destruct (rEval e2).
destruct v0.
unfold val2num.
simpl.
constructor.
inversion H0; subst.
inversion H; subst.
Defined. 


(* main result - subject reduction *)
Lemma subject_reduction_conv (e: rNum) : typPreserve e.
Proof.
unfold typPreserve.
intros t H.
induction e.
simpl.
assumption.
inversion H; subst.
apply IHe1 in H2.
apply IHe2 in H4.
simpl.
apply sumTypLemma.
assumption.
assumption. 
simpl.
assumption.
Defined. 



(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)