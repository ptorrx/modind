Paolo Torrini - KU Leuven

Modular induction framework and case study on type preservation for
the comparison of Mendler-based modular induction, MTC-style induction
and conventional induction.

Runs with Coq 8.4. Requires the -impredicative-set option.

make conv - compiles the conventional proof. 
make all - compiles the modular proofs (Mendler-style one and MTC-style one).

Functor1.v comes out of the MTC distribution (available at
http://people.csail.mit.edu/bendy/MTC/), with minor changes.
JFunctor1.v independently reimplements notions already in the MTC
distribution. Part of Functor3.v is a refactoring from the MTC
distribution.


MODULAR INDUCTION FRAMEWORK

Functors1.v - functors in Set (class Functor)
Functors2.v - Mendler induction infrastructure for Functor
Functors3.v - universality reasoning for Functor

Aux1.v, Aux2.v - auxiliary theories.

JFunctors1.v - indexed functors in K->Prop for relations
              (class JFunctor, Functor^I in the paper)
JFunctors2.v - Mendler induction for JFunctor

HFunctors1.v - indexed functors in K->Set for mutually inductive sets
              (class HFunctor)
HFunctors2.v - Mendler induction for HFunctor

JHFunctors1.v - doubly indexed functors for mutually inductive relations
              (class DFunctor)
JHFunctors2.v - Mendler induction for DFunctor


CASE STUDY

typPreserveConv.v - conventional type preservation (using non-modular datatypes)

rTyp.v - modular representation of object types

rExp.v - modular representation of object expressions

typPreserve.v - type preservation using Mendler-based modular induction

typPreserveMTC.v - type preservation using MTC-style induction


