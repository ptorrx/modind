(* Paolo Torrini, KU Leuven *)

Require Export Aux1.
Require Import rTyp.
Require Import Functors1.
Require Import Functors2.
Require Import JFunctors1.
Require Import Functors3.
Require Import rExp.

(* example of type preservation proof based on modular induction *)

Definition DVr : Set := rTyp.DVr.
Definition TVr : Set := rTyp.TVr.

Variable NN: TVr.

(* represents the type of naturals *)
Definition TNN : rTyp := TTyp (TIdT NN).


(* typing relation *)
Inductive bTyping (rTg: (rNum * rTyp) -> Prop) :
          (rNum * rTyp) -> Prop :=
  | NLitTyp : forall n: nat, bTyping rTg (NNum (NLit rNum n), TNN)
  | NSumTyp : forall (e1 e2: rNum), 
        rTg (e1, TNN) -> 
        rTg (e2, TNN) -> 
           bTyping rTg (NNum (NSum rNum e1 e2), TNN)
  | NErrTyp : bTyping rTg (NNum (NErr rNum), mIn bTyp (TBot rTyp)).

Definition rTyping : (rNum * rTyp) -> Prop := 
            JMFix (rNum * rTyp) bTyping.

Definition bTypingX : (rNum * rTyp) -> Prop := bTyping rTyping.



Definition bTyping_jfmap (B C: rNum * rTyp -> Prop) 
    (f: forall v: rNum * rTyp, B v -> C v) 
    (d: rNum * rTyp) (Aa: bTyping B d) : bTyping C d :=
    match Aa in bTyping _ d1 return bTyping C d1 with 
    | NLitTyp n => NLitTyp C n
    | NSumTyp e1 e2 prem1 prem2 =>  
            NSumTyp C e1 e2 (f (e1, TNN) prem1) (f (e2, TNN) prem2)
    | NErrTyp => NErrTyp C
    end. 


Instance bTyping_JFunctor : JFunctor (rNum * rTyp) bTyping :=
  {| jfmap := bTyping_jfmap |}.  
destruct a.
reflexivity.
reflexivity.
reflexivity.
destruct a.
reflexivity.
reflexivity.
reflexivity.
Defined.


(*********************************************)


(* Type preservation property - type_preservation *)
Definition typPreserve (e: rNum) : Prop :=
    forall t: rTyp, bTypingX (e, t) -> bTypingX (val2num (rEval e), t).


(* auxiliary lemma - sum_lemma *)
Lemma sumTypLemma (e1 e2: rNum)
         {Fun_F1: @FunctorSQU bNum bNum_Functor}    
         {Fun_F2: @FunctorSQU bTyp bTyp_Functor}: 
         bTypingX (val2num (rEval e1), TNN) -> 
         bTypingX (val2num (rEval e2), TNN)
         -> bTypingX (val2num (VSum (rEval e1) (rEval e2)), TNN).
Proof. 
intros.
destruct (rEval e1).
destruct v.
destruct (rEval e2).
destruct v0.
simpl.
constructor.
inversion H0; subst.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H2.
inversion H2.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H1.
inversion H1.
apply (@mInInvertEq bTyp bTyp_Functor Fun_F2) in H2.
inversion H2.
inversion H.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H2.
inversion H2.
apply (@mInInvertEq bNum bNum_Functor Fun_F1) in H1.
inversion H1.
apply (@mInInvertEq bTyp bTyp_Functor Fun_F2) in H2.
inversion H2.
Defined. 


(* Mendler proof algebra for the main result - SubRedMAlg *)
Lemma typPresAlg {Fun_F1: @FunctorSQU bTyp bTyp_Functor} 
                 {Fun_F2: @FunctorSQU bNum bNum_Functor}: 
           JMAlgebra rNum bIsNum typPreserve.
Proof. 
unfold JMAlgebra.
unfold JMixin.
intros R rec e E.
unfold typPreserve.
(***)
intros t H.
inversion E; subst.
simpl.
assumption.
apply rec in H0.
apply rec in H1.
unfold typPreserve in H0.
unfold typPreserve in H1.
inversion H; subst.
apply (@mInInvertEq bNum bNum_Functor Fun_F2) in H3; subst.
simpl.
rewrite H3.
assumption.
apply (@mInInvertEq bNum bNum_Functor Fun_F2) in H2; subst.
inversion H2; subst.
apply sumTypLemma.
assumption.
assumption. 
apply (jmOut (rNum * rTyp) bTyping) in H4.
apply H0 in H4.
assumption. 
apply (jmOut (rNum * rTyp) bTyping) in H5.
apply H1 in H5.
assumption. 
constructor.
simpl.
assumption. 
Defined.


(* main result - subject reduction *)
Lemma subject_reduction_mod
               {Fun_F1: @FunctorSQU bTyp bTyp_Functor}
               {tot_pred: FLiftTotal bNum (rIsNum)} :
                     forall w: rNum, typPreserve w. 
Proof.
apply (@mendlerInd bNum bNum_Functor (bNum_FunctorP1 tot_pred)). 
apply (@typPresAlg Fun_F1 (bNum_FunctorSQU1 tot_pred)).  
Defined.



(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)