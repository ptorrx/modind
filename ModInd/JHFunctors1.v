(* Paolo Torrini, KU Leuven *)

Require Import FunctionalExtensionality.

Require Import Aux2.

(* DOUBLE-INDEXED ALGEBRAS for mutually inductive relations *)
(*

Definition ddPr (I: Type) (K: I -> Type) : Type := forall (i: I), K i -> Prop.

Definition ffPr (I: Type) (K: I -> Type) : Type := 
       ddPr I K -> forall (i: I), K i -> Prop. 
*)


(* Church algebra *) 
Definition DAlgebra (I: Type) (K: I -> Type)   
  (F: ffPr I K) (A: ddPr I K) : Prop := forall (i: I) (w: K i), 
                                                      F A i w -> A i w.
(* Church fixpoint *)
Definition DFix (I: Type) (K: I -> Type) 
   (F: ffPr I K) (i: I) (w: K i) : Prop := 
      forall (A: ddPr I K), DAlgebra I K F A -> A i w.

(* Mixin *)
Definition DMixin (I: Type) (K: I -> Type) 
    (R: ddPr I K) (F: ffPr I K) (A: ddPr I K) (i: I) :=
    (forall (j:I) (w: K j), R j w -> A j w) -> 
                       (forall (w: K i), F R i w -> A i w).


(* Mendler algebra *)
Definition DMAlgebra (I: Type) (K: I -> Type) 
    (F: ffPr I K) (A: ddPr I K) : Prop :=
    forall (R: ddPr I K) (i: I), DMixin I K R F A i.

(* Mendler fixpoint *)
Definition DMFix (I: Type) (K: I -> Type) (F: ffPr I K) (i: I) (w: K i) : 
        Prop :=
    forall (A: ddPr I K), DMAlgebra I K F A -> A i w.

(* Mendler fold *) Definition dmfold (I: Type) (K: I -> Type) 
      (F: ffPr I K) : forall (A: ddPr I K) (f: DMAlgebra I K F A) 
              (i: I) (w: K i), 
           DMFix I K F i w -> A i w := fun A f i w e => e A f.


(* indexed functor class *)
Class DFunctor (I: Type) (K: I -> Type) (F : ffPr I K) :=
    {dfmap :
      forall {A B : ddPr I K} (i: I) 
                     (f : forall (j: I) (w: K j), A j w -> B j w), 
                                forall (w: K i), F A i w -> F B i w;
      dfmap_fusion :
        forall (A B C: ddPr I K) (i: I) 
                              (f : forall (j: I) (w: K j), A j w -> B j w) 
                              (g : forall (j: I) (w: K j), B j w -> C j w)
                              (w: K i) (a: F A i w),
          @dfmap B C i g w (@dfmap A B i f w a) = 
                  @dfmap A C i (fun i w e => g i w (f i w e)) w a;
      dfmap_id :
        forall (A: ddPr I K) (i: I) (w: K i) (a: F A i w),
          @dfmap A A i (fun j w => @id (A j w)) w a = a
    }.



(* In morphism *)
Definition dmIn (I: Type) (K: I -> Type) (F: ffPr I K) : 
        forall (i: I) (w: K i), F (DMFix I K F) i w -> DMFix I K F i w :=
    fun i w (fe: F (DMFix I K F) i w) 
        (A: ddPr I K) (f: DMAlgebra I K F A) 
       => f (DMFix I K F) i (dmfold I K F A f) w fe.


(* Church fold *)
Definition dfold (I: Type) (K: I -> Type) 
  (F: ffPr I K) {fun_F: DFunctor I K F}:
    forall (A: ddPr I K) (f: DAlgebra I K F A) 
           (i: I) (w: K i), DMFix I K F i w -> A i w :=
      fun A f i w (e: DMFix I K F i w) => dmfold I K F A 
                   (fun (R: ddPr I K)
                        (i: I)  
                        (rec: forall (j: I) (w: K j), R j w -> A j w) 
                        (w: K i)
                        (fa: F R i w) => f i w 
                               (@dfmap I K F fun_F R A i rec w fa)) i w e.


(* Out morphism, using Mendler fold *)
Definition dmOut (I: Type) (K: I -> Type) 
     (F : ffPr I K) {fun_F : DFunctor I K F} (i: I) (w: K i)   
        (fe: DMFix I K F i w) : F (DMFix I K F) i w :=
                fe (F (DMFix I K F)) 
                   (fun (R: ddPr I K) 
                        (i: I)
                        (rec: forall j w0, R j w0 -> (F (DMFix I K F)) j w0) 
                        (w: K i) (fa: F R i w) =>
                  (@dfmap I K F fun_F R (DMFix I K F) i 
                       (fun j w0 (x: R j w0) => 
                                  dmIn I K F j w0 (rec j w0 x)) w fa)).


(* Out morphism, using Church fold *)
Definition dmOut_C (I: Type) (K: I -> Type) 
     (F : ffPr I K) {fun_F : DFunctor I K F} : 
        forall (i: I) (w: K i), DMFix I K F i w -> F (DMFix I K F) i w :=
    fun i w => dfold I K F (F (DMFix I K F)) 
                  (fun j v => @dfmap I K F fun_F (F (DMFix I K F)) 
                         (DMFix I K F) j (dmIn I K F) v) i w.


 (* turns a Church fixpoint into a Mendler one *)
  Definition tDC2M (I: Type) (K: I -> Type) 
                (F: ffPr I K) (i: I) (w: K i) (x: DFix I K F i w) : 
                                                   DMFix I K F i w :=
    fun (C: ddPr I K) 
        (f: forall (A: ddPr I K) (i: I), 
                         (forall (j: I) (w: K j), A j w -> C j w) -> 
                         (forall (v: K i), F A i v -> C i v)) => 
                 x C (fun i => f C i (fun j w => @id (C j w))).

 (* turns a Mendler fixpoint into a Church one *)
  Definition tDM2C (I: Type) (K: I -> Type) 
                (F: ffPr I K) {fun_F: DFunctor I K F} (i: I) (w: K i)
                   (x: DMFix I K F i w) : DFix I K F i w :=
    fun (C: ddPr I K) (f: forall (i: I) (w: K i), F C i w -> C i w) =>  
           x C (fun (A: ddPr I K) i 
                    (r: forall (j: I) (w: K j), A j w -> C j w) w y => 
                                 f i w (@dfmap I K F fun_F A C i r w y)). 


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)