(* Paolo Torrini, KU Leuven *)

Require Import JFunctors1.
Require Import Aux2.
 

Lemma mendlerJInd (K: Type) (F: fPr K) {Fun_F: JFunctor K F} 
       (P: Prop) :
         (JMAlgebra K F (fun x: K => P)) -> forall w:K, JMFix K F w -> P.
Proof.
intros f w x.
apply (jmfold K F (fun x: K => P) f w).
assumption.
Defined.


Lemma mendlerJIndD (K: Type) (F: fPr K) {Fun_F: JFunctor K F} 
       (P: K -> Prop) :
         (JMAlgebra K F P) -> forall w:K, JMFix K F w -> P w.
Proof.
intros f w x.
apply (jmfold K F P f w).
assumption.
Defined.


(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)
