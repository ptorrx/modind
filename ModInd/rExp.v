(* Paolo Torrini, KU Leuven *)

Require Import FunctionalExtensionality.

Require Export Aux1.
Require Import Functors1.
Require Import Functors2.
Require Import JFunctors1.
Require Import Functors3.

(* EXPRESSIONS *)

(* Expressions are defined by bNum *)
Inductive bNum (T: Set) : Set := 
  | NLit (n: nat)
  | NSum (e1 e2: T)
  | NErr.

(* the expressions - Exp *)
Definition rNum : Set := MFix bNum.

Definition bNumT : Set := bNum rNum.


Definition NNum (x: bNumT) : rNum := mIn bNum x.

Definition NLitT (n: nat) : bNumT := NLit rNum n.

Definition NSumT (e1 e2: rNum) : bNumT := NSum rNum e1 e2.

Definition NErrT : bNumT := NErr rNum.



Definition bNum_fmap (B C: Set) (f: B -> C) (Aa : bNum B) : bNum C :=
  match Aa with 
  | NLit n     => NLit _ n
  | NSum e1 e2 => NSum _ (f e1) (f e2)
  | NErr       => NErr _
  end.


Instance bNum_Functor : Functor bNum :=
  {| fmap := bNum_fmap |}.
destruct a.
reflexivity.
reflexivity.
reflexivity.
destruct a.
reflexivity.
reflexivity.
reflexivity.
Defined.


(* characteristic predicate functor *)
Inductive bIsNum (T: rNum -> Prop) : rNum -> Prop := 
  | NLitIsNum : forall (n: nat), bIsNum T (NNum (NLitT n)) 
  | NSumIsNum : forall (e1 e2: rNum), 
            T e1 -> T e2 ->
                 bIsNum T (NNum (NSumT e1 e2))  
  | NErrIsNum : bIsNum T (NNum NErrT).


Definition rIsNum (x: rNum) : Prop := JMFix rNum bIsNum x.

Definition bIsNum_jfmap (B C: rNum -> Prop)  
       (f: forall v: rNum, B v -> C v) (d: rNum) 
       (Aa: bIsNum B d) : bIsNum C d :=
   match Aa in bIsNum _ d1 return bIsNum C d1 with 
   | NLitIsNum n => NLitIsNum C n
   | NSumIsNum e1 e2 prem1 prem2 => NSumIsNum C e1 e2 (f e1 prem1) (f e2 prem2)
   | NErrIsNum => NErrIsNum C
   end.

Instance bIsNum_JFunctor : JFunctor rNum bIsNum :=
  {| jfmap := bIsNum_jfmap |}.
destruct a.
reflexivity.
reflexivity.
reflexivity.
destruct a.
reflexivity.
reflexivity.
reflexivity.
Defined. 


(* predicatisation sigma-algebra - IsExpPA *)
Definition bNum2IsNum (* Fun_F: @FunctorSQU bTyp bTyp_Functor) *)
                    (A : Set)  
                    (rec: A -> sig rIsNum) 
                    (t: bNum A) : sig rIsNum := 
      match t in bNum _ return sig rIsNum with 
  | NLit n => @exist rNum rIsNum 
                  (NNum (NLit rNum n))   
                  (jmIn rNum bIsNum (NNum (NLit rNum n)) 
                        (NLitIsNum rIsNum n)) 
  | NSum e1 e2 => @exist rNum rIsNum _ 
                (jmIn rNum bIsNum _  
                   (NSumIsNum rIsNum
                          (proj1_sig (rec e1)) (proj1_sig (rec e2)) 
                          (proj2_sig (rec e1))
                          (proj2_sig (rec e2))
                   )
                )
  | NErr => @exist rNum rIsNum 
            (NNum (NErr rNum))
            (jmIn rNum bIsNum _ 
                  (NErrIsNum rIsNum))
  end.

Definition bNum2IsNumX (rec: rNum -> sig rIsNum) 
                    (e: bNum rNum) : sig rIsNum := bNum2IsNum rNum rec e.

Definition rNum2IsNum (e: rNum) 
                     : sig rIsNum := 
                         @mfold bNum (sig rIsNum) bNum2IsNum e.


Lemma wfNum : forall (e: bNum (sig rIsNum)),  
                      proj1_sig (bNum2IsNum (sig rIsNum) id e) = 
      mIn bNum (@fmap bNum bNum_Functor (sig rIsNum) rNum 
                                        (@proj1_sig rNum rIsNum) e).
Proof. 
intros.
destruct e.
reflexivity.
reflexivity.
reflexivity.
Defined. 

Lemma bNum2IsNum_MA_inst : 
    forall (A: Set) (e: bNum A) (rec: A -> sig rIsNum),
              bNum2IsNum A rec e = bNum2IsNum (sig rIsNum) id (fmap rec e). 
Proof.
intros.
destruct e.
reflexivity.
reflexivity.
reflexivity.
Defined.


Global Instance bNum2IsNum_MendlerAlgebra : 
        MendlerAlgebra (sig rIsNum) bNum2IsNum :=
{| ME_1 := bNum2IsNum_MA_inst
|}.


Global Instance bNum2IsNum_MPAlgebra : 
        MPAlgebra bNum rIsNum bNum2IsNum :=
{| WF_MInd := wfNum
|}.


Lemma rIsNum_total : forall (x : rNum)
      (fUP' : Universal_Property'_mfold x), rIsNum x.
Proof.
apply (@Ind bNum bNum_Functor rIsNum bNum2IsNum bNum2IsNum_MPAlgebra bNum2IsNum_MendlerAlgebra).
Defined.

Lemma rIsNum_totalG (fup: forall x: rNum, Universal_Property'_mfold x) : 
             forall (x : rNum), rIsNum x.
Proof.
intros.
apply (rIsNum_total x (fup x)).
Defined.


Instance bNum_FunctorP1 (p: FLiftTotal bNum (rIsNum))
             : @FunctorP bNum bNum_Functor :=
   {|  
       bFlift := bIsNum;
       flift_functor := bIsNum_JFunctor;
       flift_total := p
   |}.

Instance bNum_FunctorP2 (p: forall x: rNum, Universal_Property'_mfold x)
             : @FunctorP bNum bNum_Functor :=
   {|  
       bFlift := bIsNum;
       flift_functor := bIsNum_JFunctor;
       flift_total := rIsNum_totalG p
   |}.

(*********************************************)

Definition isoreqNum (w: rNum) : Prop :=
  (compose (mIn bNum) (mfold bNumT (fun (A: Set) 
                                   (r: A -> bNumT) 
                                   (a: bNum A) => 
                                       fmap (compose (mIn bNum) r) a))) w = w.

Lemma rNumIsoRec1 : JMAlgebra rNum bIsNum isoreqNum.
Proof.
unfold JMAlgebra.
unfold JMixin.
intros.
inversion H0; subst.
unfold isoreqNum.
reflexivity.
Focus 2.
unfold isoreqNum.
reflexivity.
apply H in H1.
apply H in H2.
unfold isoreqNum.
simpl.
unfold isoreqNum in H1.
unfold isoreqNum in H2.
unfold compose.
unfold mfold.
unfold mIn.
unfold NNum.
unfold mIn.
unfold mfold.
eapply (@functional_extensionality_dep Set).
intros.
eapply functional_extensionality_dep.
intros.
rewrite <- H1 at 2.
rewrite <- H2 at 2.
reflexivity.
Defined.


Lemma rNumIsoRec2 (p: FLiftTotal bNum rIsNum): forall w: rNum, isoreqNum w.
Proof.
apply (@mendlerInd bNum bNum_Functor (bNum_FunctorP1 p) isoreqNum).
apply rNumIsoRec1.
Defined.

Lemma rNum_biject2 (p: FLiftTotal bNum rIsNum): 
                     forall x: rNum, mIn bNum (mOut bNum x) = x.
Proof.
intros.
apply rNumIsoRec2.
assumption.
Defined.

Lemma rNum_biject1 (p: FLiftTotal bNum rIsNum): 
                     forall x: bNumT, mOut bNum (mIn bNum x) = x.
Proof.
apply (biject1lemmaP bNum (rNum_biject2 p)).
Defined.


Instance bNum_FunctorSQU1 (p: FLiftTotal bNum rIsNum)
             : @FunctorSQU bNum bNum_Functor :=
   {|  
       biject1 := rNum_biject1 p;
       biject2 := rNum_biject2 p
   |}.



(****************************************)


Inductive VVal : Set := 
  | VLit (v: nat).



Definition VSum (v1 v2: option VVal) : option VVal :=
  match v1 with 
  | Some (VLit x) => match v2 with 
              | Some (VLit y) => Some (VLit (x + y))
              | _ => None
              end
  | _ => None 
  end.



Definition val2natopt (v: VVal) : nat :=
  match v with 
  | VLit n => n
end.


(* evaluation function *)
Definition bEval (A: Set) (rec: A -> option VVal) (e: bNum A) : option VVal :=
  match e with
  | NLit n => Some (VLit n)
  | NSum v1 v2 => VSum (rec v1) (rec v2)
  | NErr => None 
  end. 


Definition rEval (e: rNum) : option VVal :=
  mfold (option VVal) bEval e.


Definition val2num (v: option VVal) : rNum :=
  match v with 
  | Some (VLit n) => NNum (NLit rNum n) 
  | _ => NNum (NErr rNum) 
  end.



(*****************************************)

(*
*** Local Variables: ***
*** coq-prog-args: ("-emacs-U" "-impredicative-set") ***
*** End: ***
*)